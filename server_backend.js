// @ts-check
//
//  (C) 2018 - Oliver Lenehan
//
//  Server Backend - SPX Daily Notices
//
//  SERVER  - Server Related
//  NOTICES - Notices Related
//  PATH    - Strings for file locations
//
//  Documentation available at http://127.0.0.1/doc
//
//  Server Directories
//  ./db  -> database files
//  ./img -> images for website
//  ./doc -> server web pages including docs
//
//  Minimal parts borrow from MDN docs on basic NodeJS server.
//  -> https://developer.mozilla.org/en-US/docs/Learn/Server-side/Node_server_without_framework
//

const http = require('http');
const https = require('https');
const fs = require('fs');
const pathModule = require('path');
const sqlite3 = require('sqlite3').verbose();

//const OVERWRITE_CONFIG = true;
//const OVERRIDE_CONFIG = true;

const PATH_DATABASE = './db/notices.db3';    // Notices Database Path
const PATH_LOGONDB = './db/logon.db3'

//fnl -> most likely final entry (others likely deleted) files need to be renamed... move project folder and re-organise

// Section for easy use of Server Backend
const SERVER_CONFIG_DEFAULT = {
    "OVERWRITE_CONFIG": false,
    "OVERRIDE_CONFIG": false,
    "networkPort": 80, 
    //Left term is the physical file -- Right terms are aliases for file
    "whitelist": {
        "./noticesdyn.html":        ["./", "./notices", "./notices.html"],  //fnl - needs editing
        "./client_notices_edit.js": ["./client_notices_edit.js"],           //fnl + editing script
        "./client_notices.js":      ["./client_notices.js"],                //fnl + client script
        "./dailyNotices.css":       ["./dailyNotices.css"],                 //fnl +
        "./src/school_song.ogg":    ["./school_song.ogg"],
        "./src/eddie_rice.ogg":     ["./eddie_rice.ogg"],
        "./img/civica.png":         ["./img/civica.png"],                   
        "./img/crest.png":          ["./img/crest.png"],
        "./img/thumb.png":          ["./img/thumb.png"],
        "./img/icons/PLACEHOLDER.png":["./img/icons/PLACEHOLDER.png"],
        "./img/icons/ACTIVITY.png": ["./img/icons/ACTIVITY.png"],
        "./img/icons/BASKETBALL.png":["./img/icons/BASKETBALL.png"],
        "./img/icons/CHESS.png":    ["./img/icons/CHESS.png"],
        "./img/icons/DEBATE.png":   ["./img/icons/DEBATE.png"],
        "./img/icons/FAITH.png":    ["./img/icons/FAITH.png"],
        "./img/icons/MUSIC.png":    ["./img/icons/MUSIC.png"],
        "./img/icons/ROBOTS.png":   ["./img/icons/ROBOTS.png"],
        "./img/icons/RUGBY.png":    ["./img/icons/RUGBY.png"],
        "./img/icons/SCHOOL.png":   ["./img/icons/SCHOOL.png"],
        "./img/icons/SOCCER.png":   ["./img/icons/SOCCER.png"],
        "./img/icons/SUBJECT.png":  ["./img/icons/SUBJECT.png"],
        "./img/icons/TENNIS.png":   ["./img/icons/TENNIS.png"],
        "./418.html":               ["./1337-hax0r.xyz", "./418"],
        "./img/error418.png":       ["./img/error418.png"],
        "./fonts/EmojiSymbols.woff":["./fonts/EmojiSymbols.woff"],
        "./fonts/adelle-eb.woff":   ["./fonts/adelle-eb.woff"],
        "./doc/main.html":          ["./doc"],
        "./doc/docLite_spx.css":    ["./doc/docLite_spx.css"],
        "./doc/hljs_hopscotch.css": ["./doc/hljs_hopscotch.css"],
        "./doc/highlight.pack.js":   ["./doc/highlight.pack.js"],
        "./img/markupguide.png":    ["./img/markupguide.png"],
        "./img/textmarkupexample.png":["./img/textmarkupexample.png"],
        "./img/textmarkupresult.png":["./img/textmarkupresult.png"]
    },
    "errorPages": {
        "403": "./403.html",
        "404": "./404.html",
        "418": "./418.html",
        "500": "./500.html"
    },
    "mimeTypes": {
        '': 'text/html',
        '.html': 'text/html',
        '.js': 'text/javascript',
        '.css': 'text/css',
        '.json': 'application/json',
        '.png': 'image/png',
        '.jpg': 'image/jpg',
        '.gif': 'image/gif',
        '.wav': 'audio/wav',
        '.mp3': 'audio/mp3',
        '.mp4': 'video/mp4',
        '.ogg': 'audio/ogg',
        '.woff': 'application/font-woff',
        '.ttf': 'application/font-ttf',
        '.eot': 'application/vnd.ms-fontobject',
        '.otf': 'application/font-otf',
        '.svg': 'application/image/svg+xml'
    }
}
// Creates the config file if not existing and loads defaults
var fileData;
try {
    if (JSON.parse(fs.readFileSync('./server_config.json').toString()).OVERWRITE_CONFIG) {
        fs.writeFile('./server_config.json', JSON.stringify(SERVER_CONFIG_DEFAULT, null, 4), function (error) {});
    } else if (JSON.parse(fs.readFileSync('./server_config.json').toString()).OVERWRITE_CONFIG) {
        fileData = SERVER_CONFIG_DEFAULT;
    } else {
        fileData = JSON.parse(fs.readFileSync('./server_config.json').toString());
    }
} catch (e) {
    fs.writeFile('./server_config.json', JSON.stringify(SERVER_CONFIG_DEFAULT, null, 4), function (error) {});
}


const SERVER_CONFIG = fileData || SERVER_CONFIG_DEFAULT;
const SERVER_WHITELIST = SERVER_CONFIG.whitelist; // Object for File System Whitelist
let SERVER_PORT = SERVER_CONFIG.networkPort;
const SERVER_ERROR_PAGE_LIST = SERVER_CONFIG.errorPages;
const SERVER_MIME_TYPES = SERVER_CONFIG.mimeTypes;

function SERVER_ErrorPage (response, code=500) {
    fs.readFile(SERVER_ERROR_PAGE_LIST[String(code)], function (error, content) {
        if (error) {
            response.writeHead(500, { 'Content-Type': 'text/html' });
            response.end("<h1>500 Internal Server Error. Something extra weird happened.</h1>", 'utf-8');
        } else {
            response.writeHead(code, { 'Content-Type': 'text/html' });
            response.end(content, 'utf-8');
        }
    });
}

function SERVER_Serve (response, requestPath) {
    var filePath = "." + requestPath; //used for parsing
    var allowed = false;

    //console.log(filePath);
    for (var key in SERVER_WHITELIST) {
        if (SERVER_WHITELIST[key].includes(filePath)) {
            filePath = key;
            allowed = true;
        }
    }

    //console.log("====PATH", filePath);
    fs.readFile(filePath, function (error, content) {
        if (error) {
            if (error.code == "ENOENT") {
                SERVER_ErrorPage(response, 404);
            } else {
                SERVER_ErrorPage(response, 500);
            }
        } else {
            var extname = String(pathModule.extname(filePath)).toLowerCase();
            var contentType = 'text/html';
            //var mimeTypes = SERVER_MIME_TYPES;
            contentType = SERVER_MIME_TYPES[extname] || 'application/octet-stream';
            
            //Check for whitelist
            if (allowed) {
                fs.readFile(filePath, function (error, content) {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            } else {
                SERVER_ErrorPage(response, 403);
            }
        }
    });
}

// Returns JavaScript Object
function SERVER_PostToObject (request, callback) {
    var postBody = [];
    request.on('data', function(chunk) {
        postBody.push(chunk);
    }).on('end', function() {
        var postString = Buffer.concat(postBody).toString();
        if (postString.includes("&")) {
            var params = postString.split("&");
        } else {
            var params = [postString];
        }
        var jsonObj = {};
        params.forEach(function(param) {
            var keyAndVal = param.split("=");
            jsonObj[keyAndVal[0]] = keyAndVal[1]; // 0 is the POST key and 1 is the POST value
        });
        //console.log(jsonObj);
        callback(jsonObj); //returns the POST request as an Object
    })
}

// takes input, fetch notices from selected date. client sends a request and receives array of notices
// runs sql statement on database
//
function NOTICES_Fetch (postObj, callback) {
    //console.log("POSTSTUFF", postObj);
    var clientDate = postObj.date; //should be a json date... eg yyyy-mm-ddThh:mm:ss.000Z
    clientDate = new Date(clientDate); //creates date object
    clientDate.setUTCHours(0,0,0,0); //sets time to beginning of day
    clientDate = clientDate.toJSON(); //json format for SQL
    //console.log(clientDate);
    //console.log("!!! ", clientDate);
    var noticesObj = []; // Array of notices to send to client. client then sorts and updates / adds / removes entries to update end user

    let db = new sqlite3.Database(PATH_DATABASE, function(error) {
        if (error) {
            return console.error(error.message);
        }
    });

    //db.prepare
    let sql;
    if (postObj.all) {
        // gets all future notices after the current date even if they haven't started
        sql = db.prepare(` SELECT notices.notice_id, 
                       title, 
                       message, 
                       author, 
                       year_num, 
                       start_date, 
                       end_date, 
                       subject, 
                       urgent 
                FROM   notices, 
                       notices_years 
                WHERE  notices.notice_id = notices_years.notice_id 
                       AND Date(end_date) >= Date(?, '+1 day'); `, clientDate);
    } else {
        // only gets notices that have started already and haven't ended
        sql = db.prepare(` SELECT notices.notice_id, 
                        title, 
                        message, 
                        author, 
                        year_num, 
                        start_date, 
                        end_date, 
                        subject, 
                        urgent 
                FROM   notices, 
                        notices_years 
                WHERE  notices.notice_id = notices_years.notice_id 
                        AND Date(start_date) <= Date(?, '+1 day') 
                        AND Date(end_date) >= Date(?, '+1 day'); `, clientDate, clientDate);
    }

    sql.each((err, row) => {
        //console.log("Row",row);
        var found = false;
        noticesObj.forEach(function(item){
            if (item) {
                if (item.id === row.notice_id && item.address != 0){
                    found = true;
                    item.address.push(row.year_num);
                    item.address.sort();
                    return;
                }
            }
        });
        if (!found) {
            var address;
            if (row.year_num == 0) {
                address = [5,6,7,8,9,10,11,12];
            } else {
                address = [row.year_num];
            }
            var notice = {
                'id': row.notice_id,
                "begin_date": row.start_date,
                "end_date": row.end_date,
                "title": row.title,
                "message": row.message,
                'author': row.author,
                "address": address,
                "subject": row.subject,
                'urgent': Boolean(row.urgent)
            };
            noticesObj.push(notice);
        }
        //console.log(row.notice_id, row.title, row.year_num);
    }, (error, count) => {
        sql.finalize();
    });

    db.close((err) => {
        if (err) {
            return console.error(err.message);
        }
        callback(noticesObj);
    });
}

function NOTICES_Edit(noticesObj, callback) {
    // if the last request id and user are same then overwrite in db
    //receive post data and then write to the database (check to see if record already exists)
    //console.log(noticesObj);
    var address = [];
    for (var i=5;i<13;i++) {
        if (noticesObj[`year${i}`] == "true") {
            address.push(i);
        }
    }
    var urgent = 0;
    if (noticesObj.urgent == "true") urgent = 1;
    address.sort();
    var noticeReceived = {
        'id': Number(noticesObj.id), //NEEDS TO BE DONE ON CLIENT SIDE
        "start_date": new Date(noticesObj.start_date).toJSON(),
        "end_date": new Date(noticesObj.end_date).toJSON(),
        "title": decodeURIComponent(noticesObj.title),
        "message": decodeURIComponent(noticesObj.message.replace("%0A","\n")) || "",
        'author': decodeURIComponent(noticesObj.author),
        "address": address, //FORMATTED FROM EACH RESPONSE
        "subject": noticesObj.subject || "",
        'urgent': urgent
    };
    if (address.length == 0) { callback(400); return; } //address is malformed send an error so sender knows to try again

    let db = new sqlite3.Database(PATH_DATABASE, function(error) {
        if (error) { return console.error(error.message); }
    });
    if (noticeReceived.id > -1) { // if the notice is being edited
        let sql = db.prepare(`
            UPDATE notices SET title=?, message=?, author=?, start_date=?, end_date=?, subject=?, urgent=?  
            WHERE notice_id = ?`, noticeReceived.title, noticeReceived.message, noticeReceived.author, noticeReceived.start_date, noticeReceived.end_date,
            noticeReceived.subject, noticeReceived.urgent, noticeReceived.id);
        sql.run(function(err) {
            db.run("DELETE FROM notices_years WHERE notice_id = "+ noticeReceived.id, ()=>{});
            //console.log(address);
            address.forEach(function(yearGroup) {
                let sql = db.prepare(`INSERT INTO notices_years (notice_id, year_num) VALUES (?,?)`, noticeReceived.id, yearGroup);
                sql.run(()=>{});
            });
        });
    } else { // the notice is being added for the first time
        let sql = db.prepare(`
            INSERT INTO notices (title, message, author, start_date, end_date, subject, urgent)  
            VALUES (?,?,?,?,?,?,?)`, noticeReceived.title, noticeReceived.message, noticeReceived.author, noticeReceived.start_date, noticeReceived.end_date,
            noticeReceived.subject, noticeReceived.urgent);
        sql.run(function(err) {
            var noticeID = this.lastID;
            address.forEach(function(yearGroup) {
                let sql = db.prepare(`
                    INSERT INTO notices_years (notice_id, year_num) VALUES (?,?)`, noticeID, yearGroup);
                sql.run(function(err) {});
            });
        });
    }
    db.close(function(err){ callback(200); });
}

function NOTICES_Delete(idToDelete, callback) {
    let db = new sqlite3.Database(PATH_DATABASE, function(error) {
        if (error) { return console.error(error.message); }
    });
    let sql = db.prepare(`DELETE FROM notices WHERE notice_id = ?`, idToDelete);
    sql.run(function(err){
        let sql = db.prepare("DELETE FROM notices_years WHERE notice_id = ?", idToDelete);
        sql.run(()=>{});
    });
    db.close(function(err){ callback(); });
}

function SERVER_LOGON(loginObj, callback) {
    //sessid generated from username/password/time
    //input to database with username and sessid
    //check sessid, if good then serve notices page
    //console.log(loginObj);
    let db = new sqlite3.Database(PATH_LOGONDB, function (err) {});
    //console.log("hui")
    //console.log(decodeHash(decodeURIComponent(loginObj.username)), decodeHash(decodeURIComponent(loginObj.password)));
    let sql = db.prepare(`SELECT username, author FROM users WHERE username=? AND password=?;`, decodeHash(decodeURIComponent(loginObj.username)), decodeHash(decodeURIComponent(loginObj.password)));
    sql.all(function (err, rows) {
        if (rows.length > 0) {
            callback(true, rows[0].author);
        } else {
            callback(false, "");
        }
    });
    db.close((err)=>{});
    /*
    var ldap = require('ldapjs');
    var client = ldap.createClient({
        url: 'ldap://172.16.2.104'
    });
    client.bind("SPX\\username", "password", (err)=>{
        
    });
    */
}

function encodeHash(stringToEncode="") {
    var encodedString = "";

    //ENCODING BASED ON PROGCOMP CIPHER CHALLENGE

    function inRange(num=0) {
        if (num > 126) return 31+(num-126);
        if (num < 32)  return 127-(32-num);
        return num;
    }

    const cipherConstant = 21;
    var cipherShiftBegin = 42;
    var cipherShift = cipherShiftBegin;
    var cipherBuffer = [];
    var count = 0;
    var incrementRunLength = -1;
    var resetNumber = -1;

    for (var char of stringToEncode) {
        if (count === incrementRunLength) {
            cipherShift = resetNumber
            cipherBuffer.shift();
            cipherBuffer.shift();
            count = 0;
        }
        encodedString += String.fromCharCode(inRange(char.charCodeAt(0)+cipherShift));
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);

        incrementRunLength = cipherBuffer[0];
        resetNumber = cipherBuffer[1];
        cipherShift = (cipherShift+1)%95;
        count += 1;
    }

    return encodedString;
}

function decodeHash(stringToDecode="") {
    var decodedString = "";

    //ENCODING BASED ON PROGCOMP CIPHER CHALLENGE

    function inRange(num=0) {
        if (num > 126) return 31+(num-126);
        if (num < 32)  return 127-(32-num);
        return num;
    }

    const cipherConstant = 21;
    var cipherShiftBegin = 42;
    var cipherShift = cipherShiftBegin;
    var cipherBuffer = [];
    var count = 0;
    var incrementRunLength = -1;
    var resetNumber = -1;

    for (var char of stringToDecode) {
        if (count === incrementRunLength) {
            cipherShift = resetNumber
            cipherBuffer.shift();
            cipherBuffer.shift();
            count = 0;
        }
        decodedString += String.fromCharCode(inRange(char.charCodeAt(0)-cipherShift));
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);
        cipherShiftBegin = (cipherConstant*cipherShiftBegin + 1) % 95;
        cipherBuffer.push(cipherShiftBegin);

        incrementRunLength = cipherBuffer[0];
        resetNumber = cipherBuffer[1];
        cipherShift = (cipherShift+1)%95;
        count += 1;
    }

    return decodedString;
}

function SERVER_PostResponse (request, response) {
    // if POST is sent and after it is coverted to an Object a callback is called to handle the request
    if (request.method == 'POST') { 
        //Ignores if not POSTed and continues to filesystem
        SERVER_PostToObject(request, function (postObj) {
            var filePath = "."+request.url;
            // Return a JSON File in NoticeJSON format
            // JSON is array of notices for one date
            // do nothing if request is not related to notices so it can go onto filesystem serving
            if (filePath == "./fetchnotices.spx") {
                NOTICES_Fetch(postObj, function(noticesObj) {
                    response.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
                    response.write(JSON.stringify(noticesObj), 'utf-8');
                    response.end();
                });
            } else if (filePath == "./editnotices.spx") { //still needs work
                NOTICES_Edit(postObj, (code)=> {
                    response.writeHead(code);
                    response.end();
                });
            } else if (filePath == './deletenotices.spx') {
                NOTICES_Delete(postObj.noticeid, ()=>{
                    response.writeHead(200);
                    response.end();
                });
            } else if (filePath == "./edit.spx") {
                SERVER_LOGON(postObj, (success, username)=>{
                    if (success) { //correct entry and send edit page
                        fs.readFile('notices_edit.html', function (error, content) {
                            response.setHeader("Set-Cookie", `author=${encodeURIComponent(username)}; Max-Age=2; Path=/;`);
                            response.writeHead(200, { 'Content-Type': 'text/html' });
                            response.end(content, 'utf-8');
                        });
                    } else {
                        response.writeHead(401);
                        response.end();
                    }
                });
            } else { //posted data not send to real page
                SERVER_ErrorPage(response, 418);
            }
        });
        return true;
    } else {
        return false;
    }
}


/* https test
const options = {
    key: fs.readFileSync("/srv/www/keys/my-site-key.pem"),
    cert: fs.readFileSync("/srv/www/keys/chain.pem")
  };
*/

var myArgs = process.argv.slice(2); // Command line args for ease of use of changing port on launch
if (myArgs) {
    SERVER_PORT = myArgs[0] || SERVER_CONFIG.networkPort;
}

var noticesServer = http.createServer(function (request, response) {
    //console.log(request.method, "("+request.url+")");
    var wasPosted = SERVER_PostResponse(request, response); //sees if request was through POST then handle data
    if (!wasPosted) {
        SERVER_Serve(response, request.url); //file system request
    }
    
}).listen(SERVER_PORT);
console.log('Server running on PORT:', SERVER_PORT);
process.on("uncaughtException", (error)=>{
    console.log("================================\n");
    console.log("   ERROR: UNCAUGHT EXCEPTION\n\n================================\n");
    console.log(error);
    console.log("\n================================");
});
